Meteor.startup(function () {
 // code to run on server at startup
 console.log('server startup!');
});


Meteor.methods({
   sendEmail: function (from, to, subject, body) {
      this.unblock();
      Email.send({
         from: from,
         to: to,
         subject: subject,
         text: body
      });
   },
   upsertAchievements: function (date, achievements) {
      Items.upsert({date: date}, {
         date: date,
         tasks: achievements,
         rating: -1 // TODO: allow user to rate the day
      });
      
   }
});