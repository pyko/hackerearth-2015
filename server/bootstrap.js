// if the database is empty on server start, create some sample data.
Meteor.startup(function () {
   if (Items.find().count() === 0) {
      var data = [
      {
         date: '2015-03-01',
         rating: 4,
         tasks: [
                  {text: 'Started reading "Name of the Wind"'},
                  {text: 'Watched latest episode of Orphan Black'},
                  {text: 'Practiced more of "To Zanarkand"'}
               ]
      },
      {
         date: '2015-03-02',
         rating: 2,
         tasks: [
                  {text: 'Re-watch Monsters Inc.'},
                  {text: 'Vacuum apartment + clean kitchen'},
                  {text: 'Facebook browsing'}
               ]
      },
      {
         date: '2015-03-03',
         rating: 5,
         tasks: [
                  {text: 'Started "Learn You a Haskell for Great Good"'},
                  {text: 'Learnt about veins and arteries'},
                  {text: 'Caught up with uni friends'}
               ]
      },
      {
         date: '2015-03-04',
         rating: 4,
         tasks: [
                  {text: 'More reading of "The Name of the Wind"'},
                  {text: 'More poking around with Haskell'},
                  {text: 'Cleaned the toilet'}
               ]
      },
      {
         date: '2015-03-05',
         rating: 4,
         tasks: [
                  {text: 'Watched "The Imitation Game" in the movies'},
                  {text: 'Had coffee with high school friend'},
                  {text: 'Facebook free day!'}
               ]
      },
      {
         date: '2015-03-06',
         rating: 2,
         tasks: [
                  {text: 'More practicing of "To Zanarkand"'},
                  {text: 'More reading of "The Name of the Wind"'},
                  {text: 'Random Internet browsing...'}
               ]
      },
      {
         date: '2015-03-07',
         rating: 4,
         tasks: [
                  {text: '30min exercise'},
                  {text: 'Boardgames with friends!'},
                  {text: 'Dealt with Car Insurance paperwork'}
               ]
      },
      {
         date: '2015-03-08',
         rating: 4,
         tasks: [
                  {text: '30min exercise'},
                  {text: 'Went to first Escape room - success!'},
                  {text: 'Made dinner with flatmate'}
               ]
      }
      ];

      _.each(data, function(item) {
         Items.insert(item);
      });
   }
});
