var today = moment().format('YYYY-MM-DD');

UI.registerHelper("prettifyDate", function(timestamp) {
    return moment().format('dddd D MMMM YYYY');
});

UI.registerHelper("prettifyDateShort", function(timestamp) {
    return moment().format('ddd D MMM YY');
});

Template.body.helpers({
   timestamp: function() {
      return new Date();
   }
});

Template.itemsList.helpers({
   items: function () {
      return Items.find({},{sort: {date: -1}});
   }
});

Template.itemsForm.helpers({
   today: function () {
      return Items.findOne({date: today});
   }
});


Template.body.events({
   'click #showHistory' : function (e) {
      e.preventDefault();
      $("#history").show();
      $("#blanket").show();
   },
   'click #showEmail' : function (e) {
      e.preventDefault();
      $("#send").text("Send!");
      $("#send").removeAttr("disabled");
      $("#send").css({background: '#ddd'})
      $("#from-email").val('');
      $("#to-email").val('');
      $("#email-subject").val('');
      $("#email-body").val('');
      $("#email").show();
      $("#blanket").show();
   },
   'click #showHelp' : function (e) {
      e.preventDefault();
      $("#help").show();
      $("#blanket").show();
   },
   'click .close': function (e) {
      e.preventDefault();
      $("#history").hide();
      $("#email").hide();
      $("#help").hide();
      $("#blanket").hide();
   }
});


Template.itemsForm.events({
   'blur input.item': function (event) {
      event.preventDefault();

      var items = [];
      $("#items-form .item").each(function (i, elt) {
         items.push({text: elt.value});
      });
      Meteor.call('upsertAchievements', today, items);
   },
   'keydown input.item': function (event) {
      // If enter key
      if (event.which === 13) {
         event.preventDefault();
         event.target.blur();
      }
   }
});


Template.sayThanks.events({
   'click #send': function (e) {
      e.preventDefault();
      var fromEmail = $("#from-email").val();
      var toEmail = $("#to-email").val();
      var emailSubject = $("#email-subject").val();
      var emailBody = $("#email-body").val();
      if (fromEmail && toEmail && emailSubject && emailBody) {
         Meteor.call('sendEmail',
                     fromEmail,
                     toEmail,
                     emailSubject,
                     emailBody);
         $("#send").text("Email sent :)");
         $("#send").attr("disabled", "");
         $("#send").css({background: '#aea'})   

         // Re-enable after 1.5seconds
         setTimeout(function () {
            $("#send").text("Send!");
            $("#send").removeAttr("disabled");
            $("#send").css({background: '#ddd'})
         }, 1500);
      } else {
         $("#email-info").toggle().fadeOut(2000);
      }
   }
});

Template.body.rendered = function () {
   $("body").on("keydown", function (event) {
      if (event.which === 27) {
         $("#history").hide();
         $("#email").hide();
         $("#help").hide();
         $("#blanket").hide();
      }
   });
};