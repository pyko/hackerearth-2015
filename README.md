The Looking Glass
===
The Looking Glass is a simple app that we have created for HackerEarth's 2015 International Women's Day Hackathon.

The main idea behind The Looking Glass is to encourage us to reflect on what we have achieved in the past day. Taking time to pause and reflect is something we seem to be neglecting, yet it is a simple action that is integral to improving oneself and appreciating the wonders of life.

Getting Started
===
To run the app locally (recommended) follow these four easy steps:

1. Download and install Meteor ([https://www.meteor.com/](https://www.meteor.com/))
2. Clone this repo
3. Start the app with `meteor` command
4. Go to [http://localhost:3000](http://localhost:3000) in Chrome

Live Demo
===
To see a live demo, go to: [http://the-looking-glass.meteor.com/](http://the-looking-glass.meteor.com/)

Some notes:

* Ran out of time to implement account creation/logging in, so everyone who uses this live demo will be sharing the same data
* Due to Meteor magic, on the live demo you **will** actually send an email!

Misc Notes
===
As with most hackathon code, the code here is quite horrendous, so please do not use it as an example!

The site was tested and developed on Chrome, so your mileage may vary if you try it in other browsers. Despite cutting corners on browsers, found some time to implement some basic responsiveness :)